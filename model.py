import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


dataframe=pd.read_csv('public-train.csv', delimiter='|')
dataframe_num = dataframe.copy()
encoder = LabelEncoder()

# Membuat list dari nama kolom data kategori
categorical_data = ['author_id','description','bookformat','bookedition','published_date','publisher_id','reading_age','lexile_measure',
'grade_level','genre_0','genre_1','genre_2','genre_3','genre_4','genre_5','genre_6','genre_7','genre_8','genre_9']

# Mengubah setiap data kategori menjadi numerik dengan encoder
for kolom in categorical_data:
    dataframe_num[kolom] = encoder.fit_transform(dataframe[kolom])

#for kolom in categorical_data:
   # print(kolom, dataframe[kolom].unique())

#for kolom in categorical_data:
    #print(kolom, dataframe_num[kolom].unique())

print(dataframe_num.corr())
	
# Memisahkan dataframe awal menjadi data dan label
data = dataframe_num.drop('price',axis=1)
label = dataframe_num['price']

# Memisahkan dataframe menjadi data latihan dan data tes
x_train, x_test, y_train, y_test = train_test_split(data,label,test_size=0.2)

# Print dataframe.shape untuk mengetahui bentuk dataframe
print(x_train.shape,y_train.shape)
print(x_test.shape,y_test.shape)
print(x_train)
print(x_test)
# Sekarang data sudah berupa angka sepenuhnya
plt.scatter(dataframe['genre_1_weight'], dataframe['price'])

plt.show()
print(dataframe.info())